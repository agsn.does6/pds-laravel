<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [IndexController::class,'home']);
Route::get('/data-tables', [IndexController::class, 'table']);
Route::get('/register-native', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'send']);

Route::middleware(['auth'])->group(function () {
  Route::get('/cast/create', [CastController::class, 'create']);
  Route::post('/cast', [CastController::class, 'store']);
  Route::get('/cast/{id}/edit', [CastController::class, 'edit'])->where('id', '[0-9]+');
  Route::put('/cast/{id}', [CastController::class, 'update'])->where('id', '[0-9]+');
  Route::delete('/cast/{id}', [CastController::class, 'destroy'])->where('id', '[0-9]+');
});
 
// Route::resource('/cast', CastController::class);
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show'])->where('id', '[0-9]+');


Auth::routes();
