@extends('layout.master')

@section('title')
    Halaman Cast
@endsection

@section('content')

  <a href="/cast/create" class="btn btn-primary mb-2 ml-2" tabindex="-1" role="button" aria-disabled="true">Tambah Cast Baru</a>
  <hr/>
  <section>
    <div class="row">
      @forelse ($cast as $item)
      <div class="mx-3 my-2" >
        <div class="card" style="min-heightht: 6rem; width: 18rem; background-color: gainsboro">
          <div class="card-body">
            <h5 class="card-title"><b>{{$item->nama}}</b></h5><br/>
            <h6 class="card-title">{{$item->umur}} Tahun</h6><br/><br/>
            <p class="card-text">{{ Str::limit($item->bio, 100) }}</p>
            
            <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Lihat Detail Saya</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Update</a>
            <form action="/cast/{{$item->id}}" method="POST" style="display: inline">
              @csrf
              @method('delete')
              <button type="submit" class="btn btn-sm btn-danger">Delete</a>
            </form>
          </div>
        </div>
      </div>
      @empty
        <div class="ml-3" style="display: inline-block">
          <h2> Belum ada data</h2>
          <p> Klik 'Tambah Cast Baru' untuk menambahkan data cast</p>
        </div>
      @endforelse
    </div>
  </section>
@endsection
   
