@extends('layout.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('content')
<div class="card">
  <div class="card-body">
    <h5 class="card-title"><b>{{$cast->nama}}</b></h5><br/>
    <h6 class="card-title">{{$cast->umur}} Tahun</h6><br/><br/>
    <span class="card-text"><b>Biografi Cast:</b></span>
    <p class="card-text">{{ $cast->bio }}</p>
    
    <a href="/cast" class="btn btn-sm btn-info btn-block">Kembali</a>
  </div>
</div>
@endsection
