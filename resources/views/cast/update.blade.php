@extends('layout.master')

@section('title')
    Form Update Cast
@endsection

@section('content')
  <section>
    <form action="/cast/{{$cast->id}}" method="POST">
      @csrf
      @method('put')
      <div class="form-floating mb-3">
        <label for="floatingInput">Nama</label>
        <input type="text" class="form-control" id="floatingInput" placeholder="Tuliskan nama lengkap" name="nama" value="{{$cast->nama}}">
      </div>
      @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
      <div class="form-floating mb-3">
        <label for="floatingInput">Umur</label>
        <input type="number" class="form-control" id="floatingPassword" placeholder="Masukkan usia dengan angka" name="umur" value="{{$cast->umur}}">
      </div>
      @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
      <div class="form-floating mb-5">
        <label for="floatingTextarea2">Biografi</label>
        <textarea class="form-control" placeholder="Tuliskan biografi singkat" id="floatingTextarea2" style="height: 100px" name="bio">{{$cast->bio}}</textarea>
      </div>
      @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </section>
@endsection
