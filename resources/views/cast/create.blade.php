@extends('layout.master')

@section('title')
    Form Create Cast
@endsection

@section('content')
  <section>
    <form action="/cast" method="POST">
      @csrf
      <div class="form-floating mb-3">
        <label for="floatingInputNama">Nama</label>
        <input type="text" class="form-control" id="floatingInputNama" placeholder="Tuliskan nama lengkap" name="nama">
      </div>
      @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
      <div class="form-floating mb-3">
        <label for="floatingInputUmur">Umur</label>
        <input type="number" class="form-control" id="floatingInputUmur" placeholder="Masukkan usia dengan angka" name="umur">
      </div>
      @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror
      <div class="form-floating mb-5">
        <label for="floatingTextarea2">Biografi</label>
        <textarea class="form-control" placeholder="Tuliskan biografi singkat" id="floatingTextarea2" style="height: 100px" name="bio"></textarea>
      </div>
      @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
      @enderror

      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </section>
@endsection
