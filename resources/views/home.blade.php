@extends('layout.master')

@section('title')
    Halaman Home
@endsection

@section('content')
  <header>
    <h1>Media Online</h1>
  </header>

  <article>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
  </article>

  <section>
    <h3>Benefit Join di Media Online</h3>
    <ul>
      <li>Mendapatkan motivasi dari sesama para Developer</li>
      <li>Sharing knowledge</li>
      <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
  </section>

  <section>
    <h3>Cara Bergabung ke Media Online</h3>
    <ol>
      <li>Mengunjungi Website ini</li>
      <li>Mendaftarkan di <a href="/register-native"><b>Form Sign Up</b></a></li>
      <li>Selesai</li>
    </ol>
  </section>
@endsection
   
