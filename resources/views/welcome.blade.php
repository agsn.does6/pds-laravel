@extends('layout.master')

@section('title')
    Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$fname}} {{$lname}}</h1>
    <h4>
    Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!
    </h4>
@endsection
