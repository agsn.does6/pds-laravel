@extends('layout.master')

@section('title')
    Halaman Form
@endsection

@section('content')
  <header>
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
  </header>
  <section>
    <form action="/welcome" method="POST">
      @csrf
      <label>First Name : </label>
      <br />
      <input type="text" name="firstname" />
      <br />
      <label>Last Name : </label>
      <br />
      <input type="text" name="lastname" />
      <br /><br />
      <label>Gender </label>
      <br />
      <input type="radio" value="0" name="gender" />Male<br />
      <input type="radio" value="1" name="gender" />Female<br />
      <br />
      <label>Nationality </label>
      <br />
      <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="amerika">Amerika</option>
        <option value="inggris">Inggris</option>
      </select>
      <br /><br />
      <label>Language Spoken</label>
      <br />
      <input type="checkbox" name="language" value="bahasa" />Bahasa
      Indonesia<br />
      <input type="checkbox" name="language" value="english" />English<br />
      <input type="checkbox" name="language" value="other" />Other<br />
      <br />
      <label>Bio</label>
      <br />
      <textarea name="shortbio" cols="30" rows="10"></textarea>
      <br />
      <button type="submit">Sign Up</button>
    </form>
  </section>
@endsection
